var express = require('express');
var model = require('../models/news');
var fileModel = require('../models/fileupload');

var router = express.Router();


/* GET news listing. */
router.get('/related', function (req, res, next) {
    getRelatedNews(req, res);
});

router.get('/:lang', function (req, res, next) {
    getNews(req, res);
});

// NEWS FUNCTIONALITY

function getNews(req, res) {
    var lang = req.params.lang;
    model.aggregate([
        {
            $lookup: {
                from: 'upload_file',
                localField: '_id',
                foreignField: 'related.ref',
                as: 'images'
            }
        },
        {
            $project: {
                title: `$title_${lang}`,
                shortDescription: `$short_description_${lang}`,
                description: `$description_${lang}`,
		createdAt: '$createdAt',
		images: {
			"$map": { "input": "$images", "as": "im", "in": {url: "$$im.url", type: '$$im.related.field'} }
		}
            }
        }
    ]).exec().then(data => {
        res.status(200).send(data);
    }).catch((err) => {
console.log(err)
        res.status(404).send({ succes: false });
    });
}

function getNewsById(req, res) {
    var id = req.params.id;
    model.find({ id: id }, function (err, response) {
        if (response) {
            res.status(200).send(response[0]);
            return;
        }
        console.log(err);
        res.status(404).send({ success: false });
    });
}

function getRelatedNews(req, res) {
    model.find({}).sort({ date: -1 }).limit(10).exec(function (err, response) {
        if (response) {
            res.status(200).send(response);
            return;
        }
        console.log(err);
        res.status(404).send({ success: false });
    });
}

module.exports = router;

