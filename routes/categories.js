var express = require('express');
var model = require('../models/categories');
var fileModel = require('../models/fileupload');

var router = express.Router();


/* GET categories listing. */
router.get('/:lang', function (req, res, next) {
    getCategories(req, res);
});

// CATEGORIES FUNCTIONALITY

function getCategories(req, res) {
    var lang = req.params.lang;
    fileModel.aggregate([
        { $unwind: '$related' },
        { $match: { 'related.kind': 'Categories' } },
        {
            $lookup: {
                from: 'categories',
                localField: 'related.ref',
                foreignField: '_id',
                as: 'category'
            }
        },
        { $unwind: '$category' },
        {
            $project: {
		_id: 0,
		categoryId: '$category._id',
		name: `$category.name_${lang}`,
		description: `$category.description_${lang}`,
                image: '$url',
		createdAt: '$category.createdAt'
            }
        },
	{ $sort: { createdAt: 1 } }
    ]).exec().then(data => {
        res.status(200).send(data);
    }).catch((err) => {
        res.status(404).send({ succes: false });
    });
}


module.exports = router;

