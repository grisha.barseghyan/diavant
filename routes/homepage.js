var express = require('express');
var model = require('../models/homepage');

var router = express.Router();

/* GET homepage. */
router.get('/:lang', function (req, res, next) {
    getHomepageViaLang(req, res);
});

// HOMEPAGE FUNCTIONALITY

function getHomepageViaLang(req, res) {
    var lang = req.params.lang;
    model.aggregate([{
        $project: {
            products_description: `$products_description_${lang}`,
            why_us_first_field: `$why_us_first_field_${lang}`,
            why_us_first_field_description: `$why_us_first_field_description_${lang}`,
            why_us_second_field: `$why_us_second_field_${lang}`,
            why_us_second_field_description: `$why_us_second_field_description_${lang}`,
            why_us_third_field: `$why_us_third_field_${lang}`,
            why_us_third_field_description: `$why_us_third_field_description_${lang}`,
            why_us_forth_filed: `$why_us_forth_filed_${lang}`,
            why_us_forth_filed_description: `$why_us_forth_filed_description_${lang}`,
            why_us_title: `$why_us_title_${lang}`,
	    news_description: `$news_description_${lang}`
        }
    }]).exec().then(data => {
        res.status(200).send(data[0]);
    }).catch(() => {
        res.status(404).send({ succes: false });
    });
}

module.exports = router;

