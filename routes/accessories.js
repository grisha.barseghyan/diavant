var express = require('express');
var model = require('../models/accessories');
var productModel = require('../models/products');
var mongoose = require('mongoose');

var router = express.Router();

/* GET accessories page. */
router.get('/:lang/:productId', function (req, res, next) {
    getAccessoriesWithLang(req, res);
});

// ACCESSORIES FUNCTIONALITY

function getAccessoriesWithLang(req, res) {
    var lang = req.params.lang;
    var productId = req.params.productId;
    productModel.find({ _id: productId }, 'accessories', (err, response) => {
        if (err) {
            res.status(404).send({ success: false });
        }
        console.log('response: ', response);
        model.aggregate([
            {
                $project: {
                    name: `$name_${lang}`
                }
            }
        ]).exec().then(data => {
            data = data.filter(el => response[0].accessories.indexOf(el._id) >= 0);
            res.status(200).send(data)
        }).catch(err => {
            console.log(err);
            res.status(404).send({ success: false });
        });
    });
}

module.exports = router;

