var express = require('express');
var fileModel = require('../models/fileupload');

var router = express.Router();

/* GET partners listing. */
router.get('/', function (req, res, next) {
    getPartners(req, res)
});

// PARTNERS FUNCTIONALITY

function getPartners(req, res) {
    fileModel.aggregate([
	{ $unwind: '$related' },
	{ $match: { 'related.kind': 'Partners' } },
	{
	    $lookup: {
		from: 'partners',
		localField: 'related.ref',
		foreignField: '_id',
		as: 'partner'
	    }
	},
	{
	    $project: {
		_id: 0,
		url: 1
	    }
	}
    ]).exec().then(data => {
	res.status(200).send(data);
    }).catch((err) => {
        res.status(404).send({ succes: false });
    });
}

module.exports = router;
