var express = require('express');
var model = require('../models/products');
var router = express.Router();
var mongoose = require('mongoose');

/* GET search page. */
router.get('/text/:lang/:search_string', function (req, res, next) {
    searchByText(req, res);
});

router.get('/brand/:lang/:brand', function (req, res, next) {
    searchByBrand(req, res);
});

// SEARCH FUNCTIONALITY

function searchByText(req, res) {
    var search_string = req.params.search_string;
    var lang = req.params.lang;
    model.aggregate([
        { $match: { $text: { $search: search_string } } },
        { $sort: { score: { $meta: "textScore" } } },
        {
            $lookup: {
                from: 'upload_file',
                localField: '_id',
                foreignField: 'related.ref',
                as: 'images'
            },
        },
        {
            $project: {
                images: {
                    "$map": { "input": "$images", "as": "im", "in": { url: "$$im.url", type: '$$im.related.field' } }
                },
                name: `$name_${lang}`,
                shortDescription: `$short_description_${lang}`
            }
        }
    ]).exec().then(data => {
        res.status(200).send(data);
    }).catch(err => {
        console.log(err);
        res.status(404).send({ success: false });
    });
}

function searchByBrand(req, res) {
    var brand = req.params.brand;
    var lang = req.params.lang;
    model.aggregate([
        { $match: { 'brand': mongoose.Types.ObjectId(brand) } },
        {
            $lookup: {
                from: 'upload_file',
                localField: '_id',
                foreignField: 'related.ref',
                as: 'images'
            },
        },
        {
            $project: {
                images: {
                    "$map": { "input": "$images", "as": "im", "in": { url: "$$im.url", type: '$$im.related.field' } }
                },
                name: `$name_${lang}`,
                shortDescription: `$short_description_${lang}`
            }
        }
    ]).exec().then(data => {
        res.status(200).send(data);
    }).catch(err => {
        console.log(err);
        res.status(404).send({ success: false });
    });
}

module.exports = router;

