var express = require('express');
var model = require('../models/subscribers');

var router = express.Router(); 

/* POST brands page. */
router.post('/', function(req, res, next) {
  postSubscribers(req, res);
});

// SUBSCRIBERS FUNCTIONALITY

function postSubscribers(req, res) {
    var subscriber = req.body;
    model.create(subscriber, function(err, response) {
        if (response){
            res.status(200).send({success: true});
            return;
        };
        console.log(err)
        res.status(500).send({success: false});
    });
}

module.exports = router;
