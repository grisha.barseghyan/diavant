var express = require('express');
var model = require('../models/software');
var productModel = require('../models/products');

var router = express.Router();

/* GET soft page. */
router.get('/:lang/:productId', function (req, res, next) {
    getSoftWithLang(req, res);
});

// SOFTWARE FUNCTIONALITY

function getSoftWithLang(req, res) {
    var lang = req.params.lang;
    var productId = req.params.productId;
    productModel.find({ _id: productId }, 'softwares', (err, response) => {
        if (err) {
            res.status(404).send({ success: false });
        }
        model.aggregate([
            {
                $project: {
                    name: `$name_${lang}`
                }
            }
        ]).exec().then(data => {
            data = data.filter(el => response[0].softwares.indexOf(el._id) >= 0);
            res.status(200).send(data)
        }).catch(err => {
            console.log(err);
            res.status(404).send({ success: false });
        });
    });
}

module.exports = router;

