var express = require('express');
var mailer = require('../utilities/mailer');
var model = require('../models/aboutpage');

var router = express.Router();

/* GET aboutpage. */
router.get('/:lang', function (req, res, next) {
    getAboutpageViaLang(req, res);
});

/* POST aboutpage message */
router.post('/message', function (req, res, next) {
    sendMessage(req, res);
});

router.post('/price', function (req, res, next) {
    sendPriceRequest(req, res);
});

// ABOUTPAGE FUNCTIONALITY

function getAboutpageViaLang(req, res) {
    var lang = req.params.lang;
    model.aggregate([
        {
            $lookup: {
                from: 'upload_file',
                localField: '_id',
                foreignField: 'related.ref',
                as: 'images'
            }
        },
        {
            $project: {
                slider: 1,
                certiicates: 1,
                video: 1,
                images: {
                    "$map": { "input": "$images", "as": "im", "in": { url: "$$im.url", type: '$$im.related.field' } }
                },
                description: `$description_${lang}`,
                mission: `$mission_${lang}`,
                experience: `$experience_${lang}`,
                equipment: `$equipment_${lang}`,
                prices: `$prices_${lang}`
            }
        }
    ]).exec().then(data => {
        res.status(200).send(data[0]);
    }).catch(() => {
        res.status(404).send({ succes: false });
    });
}

function sendMessage(req, res) {
    var message = req.body;
    mailer.sendMessage(message)
        .then(() => {
            res.status(200).send({ success: true });
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({ success: false });
        });
}

function sendPriceRequest(req, res) {
    var message = req.body;
    mailer.sendPriceRequest(message)
        .then(() => {
            res.status(200).send({ success: true });
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({ success: false });
        });
}

module.exports = router;
