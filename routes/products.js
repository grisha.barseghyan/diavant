var express = require('express');
var model = require('../models/products');
var fileModel = require('../models/fileupload');
var mongoose = require('mongoose');

var router = express.Router();

/* GET products listing. */


router.get('/byCategory/:lang', function (req, res, next) {
  getProductsByCategory(req, res);
});

router.get('/bycategoryandbrand/:lang/:categoryId/:brandId', function (req, res, next) {
  getProductsByCategoryAndBrand(req, res);
})

router.get('/:lang/:id', function (req, res, next) {
  getProductsById(req, res);
});


router.get('/related/:lang/:productId/:categoryId', function (req, res, next) {
  getRelatedProducts(req, res);
});

// PRODUCTS FUNCTIONALITY
function getProductsByCategory(req, res) {
  var lang = req.params.lang;
  var category = req.query.category;
  var index = Number(req.query.index);
  var limit = Number(req.query.limit);
  model.aggregate([
    { $match: {
      'category': mongoose.Types.ObjectId(category),
    } },
    { $skip: index },
    { $limit: limit },
    { $lookup: {
      from: 'upload_file',
      localField: '_id',
      foreignField: 'related.ref',
      as: 'images'
    } },
    { '$project': {
      images: {
        "$map": { "input": "$images", "as": "im", "in": { url: "$$im.url", type: '$$im.related.field' } }
      },
        name: `$name_${lang}`,
        shortDescription: `$short_description_${lang}`
    }}
  ]).exec().then(data => {
    res.status(200).send(data.reverse());
  }).catch(err => {
    console.log(err);
    res.status(404).send({ success: false });
  })
}

function getProductsByCategoryAndBrand(req, res) {
  var lang = req.params.lang;
  var categoryId = req.params.categoryId;
  var brandId = req.params.brandId;
  fileModel.aggregate([
    { $unwind: '$related' },
    {
      $match: {
        'related.kind': 'Products',
        'related.field': 'thumbnail'
      }
    },
    {
      $lookup: {
        from: 'products',
        localField: 'related.ref',
        foreignField: '_id',
        as: 'product'
      }
    },
    { $unwind: '$product' },
    {
      $match: {
        'product.category': mongoose.Types.ObjectId(categoryId),
        'product.brand': mongoose.Types.ObjectId(brandId)
      }
    },
    {
      $project: {
	_id: '$product.id',
        image: '$url',
        name: `$product.name_${lang}`,
        shortDescription: `$product.short_description_${lang}`
      }
    }
  ]).exec().then(data => {
    res.status(200).send(data);
  }).catch(err => {
    console.log(err);
    res.status(404).send({ success: false });
  });
}

function getProductsById(req, res) {
  var lang = req.params.lang;
  var id = req.params.id;
  model.aggregate([
    {
      $match: {
        'id': id
      }
    },
    {
      $lookup: {
        from: 'upload_file',
        localField: '_id',
        foreignField: 'related.ref',
        as: 'images'
      }
    },
    {
      $project: {
        name: `$name_${lang}`,
        description_part_one: `$description_part_one_${lang}`,
        description_part_two: `$description_part_two_${lang}`,
        description_part_three: `$description_part_three_${lang}`,
	category: '$category',
        images: {
          "$map": { "input": "$images", "as": "im", "in": { url: "$$im.url", type: '$$im.related.field' } }
        }
      }
    }
  ]).exec().then(data => {
    res.status(200).send(data);
  }).catch(err => {
    console.log(err);
    res.status(404).send({ success: false });
  });
}

function getRelatedProducts(req, res) {
  var lang = req.params.lang;
  var productId = req.params.productId;
  var categoryId = req.params.categoryId;
  fileModel.aggregate([
    { $unwind: '$related' },
    {
      $match: {
        'related.kind': 'Products',
        'related.field': 'thumbnail'
      }
    },
    {
      $lookup: {
        from: 'products',
        localField: 'related.ref',
        foreignField: '_id',
        as: 'product'
      }
    },
    { $unwind: '$product' },
    {
      $match: {
        'product.category': mongoose.Types.ObjectId(categoryId),
        'product.id': { $ne: productId }
      }
    },
    {
      $project: {
	_id: '$product.id',
        image: '$url',
        name: `$product.name_${lang}`,
        shortDescription: `$product.short_description_${lang}`
      }
    }
  ]).exec().then(data => {
    res.status(200).send(data);
  }).catch(err => {
    console.log(err);
    res.status(404).send({ success: false });
  });
}

module.exports = router;

