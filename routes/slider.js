var express = require('express');
var model = require('../models/slider');
var fileModel = require('../models/fileupload');

var router = express.Router();

/* GET slider data. */
router.get('/', function (req, res, next) {
    getSliderData(req, res);
});

// SLIDER FUNCTIONALITY

function getSliderData(req, res) {
    fileModel.aggregate([
        {
            $match: {
                'related.kind': 'Products',
                'related.field': 'slider_image',
            }
        },
        {
            $lookup: {
                from: 'products',
                localField: 'related.ref',
                foreignField: '_id',
                as: 'product'
            }
        },
        { $unwind: '$product' },
	{ $lookup: {
		from: 'slider',
		localField: 'product._id',
		foreignField: 'product',
		as: 'slider'
	  }
	},
        { $unwind: '$slider' },
        {
            $project: {
		_id: '$slider._id',
                image: '$url',
                productId: '$product._id',
                createdAt: '$slider.createdAt'
            }
        },
        { $sort: { createdAt: 1 } }
    ]).exec().then(data => {
        res.status(200).send(data);
    }).catch(err => {
        console.log(err);
        res.status(404).send({ success: false });
    });
/*    fileModel.aggregate([
        { $unwind: '$related' },
        {
            $match: {
                'related.kind': 'Products',
                'related.field': 'slider_image'
            }
        },
        {
            $lookup: {
                from: 'products',
                localField: 'related.ref',
                foreignField: '_id',
                as: 'product'
            }
        },
        { $unwind: '$product' },
        {
            $project: {
                image: '$url',
                productId: '$product._id',
		createdAt: '$createdAt'
            }
        },
	{ $sort: { _id: 1 } }
    ]).exec().then(data => {
        res.status(200).send(data);
    }).catch(err => {
        console.log(err);
        res.status(404).send({ success: false });
    });*/
}

module.exports = router;

