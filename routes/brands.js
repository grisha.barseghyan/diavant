var express = require('express');
var model = require('../models/brands');

var router = express.Router();

/* GET brands page. */
router.get('/:lang', function (req, res, next) {
    getBrandsWithLang(req, res);
});

// BRANDS FUNCTIONALITY

function getBrandsWithLang(req, res) {
    var lang = req.params.lang;
    model.aggregate([
        {
            $lookup: {
                from: 'upload_file',
                localField: '_id',
                foreignField: 'related.ref',
                as: 'image'
            }
        },
        {
            $project: {
                name: `$name_${lang}`,
                longName: `$long_name_${lang}`,
                description: `$description_${lang}`,
                image: '$image.url'
            }
        }
    ]).exec().then(data => {
        res.status(200).send(data);
    }).catch((err) => {
        res.status(404).send({ succes: false });
    });
}

module.exports = router;

