var express = require('express');
var model = require('../models/contactpage');

var router = express.Router();

/* GET homepage. */
router.get('/:lang', function (req, res, next) {
    getContactpageViaLang(req, res);
});

// HOMEPAGE FUNCTIONALITY

function getContactpageViaLang(req, res) {
    var lang = req.params.lang;
    model.aggregate([{
        $project: {
            description: `$description_${lang}`,
            contact: `$contact_${lang}`,
            location: `$location_${lang}`,
            hours: `$hours_${lang}`
        }
    }]).exec().then(data => {
        res.status(200).send(data[0]);
    }).catch(() => {
        res.status(404).send({ succes: false });
    });
}

module.exports = router;
