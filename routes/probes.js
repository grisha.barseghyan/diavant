var express = require('express');
var fileModel = require('../models/fileupload');
var productModel = require('../models/products');
var model = require('../models/probes');

var router = express.Router();

/* GET probes page. */
router.get('/:lang/:productId', function (req, res, next) {
    getProbesWithLang(req, res);
});

// PROBES FUNCTIONALITY

function getProbesWithLang(req, res) {
    var lang = req.params.lang;
    var productId = req.params.productId;
    productModel.find({ _id: productId }, 'probes', (err, response) => {
        if (err) {
            res.status(404).send({ success: false });
	    return;
        }
        fileModel.aggregate([
            { $unwind: '$related' },
            {
                $match: {
                    'related.kind': 'Probes',
                    'related.field': 'image'
                }
            },
            {
                $lookup: {
                    from: 'probes',
                    localField: 'related.ref',
                    foreignField: '_id',
                    as: 'probe'
                }
            },
            { $unwind: '$probe' },
            {
                $project: {
                    name: `$probe.name_${lang}`,
		    _id: '$probe._id',
                    hertz: `$probe.hertz_${lang}`,
                    length: `$probe.length_${lang}`,
                    image: '$url'
                }
            }
        ]).exec().then(data => {
            data = data.filter(el => response[0].probes.indexOf(el._id) >= 0);
            res.status(200).send(data)
        }).catch(err => {
            console.log(err);
            res.status(404).send({ success: false });
        });
    });
}

module.exports = router;

