var express = require('express');
var model = require('../models/footer');

var router = express.Router();

/* GET footer. */
router.get('/:lang', function (req, res, next) {
    getFooterWithLang(req, res);
});

// HOMEPAGE FUNCTIONALITY

function getFooterWithLang(req, res) {
    var lang = req.params.lang;
    model.aggregate([{
        $project: {
            description: `$description_${lang}`,
            address: `$address_${lang}`,
            city: `$city_${lang}`,
            first_phone: 1,
            second_phone: 1,
            email: 1
        }
    }]).exec().then(data => {
        res.status(200).send(data[0]);
    }).catch(() => {
        res.status(404).send({ succes: false });
    });
}

module.exports = router;
