'use strict';
const nodemailer = require('nodemailer');
const subscribers = require('../models/subscribers');

// Connecting to existing gmail account
var transporter = nodemailer.createTransport({
 service: 'gmail',
 auth: {
        // TODO: user, pass
    }
});

module.exports = {
    sendMessage: function(message) {
        const messageSubject = `Diavant new message`;
        const messageBody = `Diavant new message:
        <br>
        <p>Name: ${message.name}</p>
        <p>Email: ${message.email}</p>
        <p>Subject: ${message.subject}</p>
        <p>Message: ${message.message}</p>
        <br>
        <p>Regards,</p>
        <p>Diavant automated mail system.</p>`;

        return sendMailToAdmin(messageSubject, messageBody)
    },

    sendPriceRequest: function(message) {
        const messageSubject = `New price request`;
        const messageBody = `New price request received
            <br>
            <p>Brand: ${message.selectedBrand.name_en}</p>
            <p>Category: ${message.selectedCategory.name_en}</p>
            <p>Product: ${message.selectedProduct.name_en}</p>
            <p>Probes: ${ getSelectedItemsNames(message.selectedProbes) }</p>
            <p>Software: ${ getSelectedItemsNames(message.selectedSoftware) }</p>
            <p>Accessories: ${ getSelectedItemsNames(message.selectedAccessories) }</p>
            <br>
            <p>Contact person information</p>
            <p>Name: ${message.name}</p>
            <p>Email: ${message.email}</p>
            <p>Phone: ${message.phone}</p>
            <p>Message: ${message.message}</p>
            <br>
            <p>Regards,</p>
            <p>Diavant automated mail system.</p>
        `;
        return sendMailToAdmin(messageSubject, messageBody)
    },

    newProductMessage: function(product) {
        const messageSubject = `Diavant new product`;
        const messageBody = `Diavant new product:
        <br>
        <p>${product.name_en}</p>
        <br>
        <p>Regards,</p>
        <p>Diavant automated mail system.</p>`;

        sendMail(messageSubject, messageBody)
    },

    newsMessage: function(news) {
        const messageSubject = `Diavant newslatter`;
        const messageBody = `Be informed about Diavant news:
        <br>
        <p>${news.title_en}</p>
        <br>
        <p>Regards,</p>
        <p>Diavant automated mail system.</p>`;

        sendMail(messageSubject, messageBody)
    }
};

function sendMailToAdmin(subject, body) {
    const mailOptions = {
        from: 'diavant@gmail.com',
        to: 'diavant@gmail.com',
        subject: subject,
        html: body
    };

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, function (err, info) {
            if(err){
                reject(err);
            }else{
                console.log(info);
                resolve();
            };
        });
    });
    
}

// Function to fill message options and send
function sendMail(subject, body) {
    getSubscribers()
        .then(subscribers => {
            subscribers.forEach(sub => {
                const mailOptions = {
                    from: 'diavant@gmail.com',
                    to: sub.email,
                    subject: subject,
                    html: body
                };
                transporter.sendMail(mailOptions, function (err, info) {
                    if(err){
                        console.log(err);
                    }else{
                        console.log(info);
                    };
                });
            });
        })
        .catch(err => {
            console.log(err);
        });
};

function getSubscribers() {
    return new Promise((resolve, reject) => {
        subscribers.find({}, function(err, response){
            if (response) {
                resolve(response);
                return;
            }
            console.log(err);
            reject(err);
        });
    });
}

function getSelectedItemsNames (items) {
    var names = '';
    items.map(el => {
        names += `<p>${el.name_en}</p>`;
    });
    return names;
}
