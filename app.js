var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var request = require('request');
var thumbnail = require('image-thumbnail');
var btoa = require('btoa');

// ROUTERS
var subscribersRoute = require('./routes/subscribers');
var indexRouter = require('./routes/index');
var partnersRouter = require('./routes/partners');
var categoriesRouter = require('./routes/categories');
var productsRouter = require('./routes/products');
var sliderRouter = require('./routes/slider');
var newsRouter = require('./routes/news');
var brandsRouter = require('./routes/brands');
var searchRouter = require('./routes/search');
var probesRouter = require('./routes/probes');
var softRouter = require('./routes/software');
var accRouter = require('./routes/accessories');

// ROUTES OF PAGES 
var aboutpageRouter = require('./routes/aboutpage');
var contactpageRouter = require('./routes/contactpage');
var footerRouter = require('./routes/footer');
var homeRouter = require('./routes/homepage');

// MONGO DB
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/strapi', { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, function (err) {
  if (err) {
    console.log('Connection error: ', err)
  } else {
    console.log('Conneted to Diavant database.')
  }
});
mongoose.Promise = require('bluebird');

var app = express();

app.listen(3000, function(){
    console.log('Connected to port:', this.address().port);
});

app.use(logger('dev'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'strapi/public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.use('/images/uploads', async function (req, res) {
  var filename = req.url.split('?size=')[0];
  var sizes = req.url.split('?size=')[1];
  var width = Number(sizes.split('x')[0]);
  var height = Number(sizes.split('x')[1]);
  var options = { width: width, height: height, responseType: 'base64' };
  var thumb = await thumbnail(`strapi/public/uploads${filename}`, options)
  var img = new Buffer(thumb, 'base64');
  res.writeHead(200, {
    'Content-Type': 'image/png',
    'Content-Length': img.length
  });
  res.end(img);
});

app.use('/apiV2/subscribers', subscribersRoute);
app.use('/apiV2/aboutpage', aboutpageRouter);
app.use('/apiV2/contactpage', contactpageRouter);
app.use('/apiV2/footer', footerRouter);
app.use('/apiV2/homepage', homeRouter);
app.use('/apiV2/partners', partnersRouter);
app.use('/apiV2/categories', categoriesRouter);
app.use('/apiV2/products', productsRouter);
app.use('/apiV2/slider', sliderRouter);
app.use('/apiV2/news', newsRouter);
app.use('/apiV2/brands', brandsRouter);
app.use('/apiV2/search', searchRouter);
app.use('/apiV2/probes', probesRouter);
app.use('/apiV2/softwares', softRouter);
app.use('/apiV2/accessories', accRouter);

app.use('/api', function (req, res) {
  var url = 'http://localhost:1337/api' + req.url;
  req.pipe(request(url)).pipe(res);
});

app.all('*', function (req, res) {
  res.status(200).sendFile(__dirname + '/public/index.html');
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
