var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  description_en: { type: String, required: true },
  address_en: { type: String, required: true },
  city_en: { type: String, required: true },
  first_phone: { type: String, required: true },
  second_phone: { type: String, required: true },
  email: { type: String, required: true },
},
  {
    collection: 'footer'
  });

module.exports = mongoose.model('footer', schema, 'footer');