var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  id: { type: String, required: true },
  title_en: { type: String, required: true },
  title_ru: { type: String, required: true },
  title_hy: { type: String, required: true },
  short_description_en: { type: String, required: true },
  short_description_ru: { type: String, required: true },
  short_description_hy: { type: String, required: true },
  description_en: { type: String, required: true },
  description_ru: { type: String, required: true },
  description_hy: { type: String, required: true },
  createdAt: { type: Date, required: true },
},
  {
    collection: 'news'
  });

module.exports = mongoose.model('news', schema, 'news');
