var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  description_en: { type: String, required: true },
  description_ru: { type: String, required: true },
  description_hy: { type: String, required: true },
  contact_en: { type: String, required: true },
  contact_ru: { type: String, required: true },
  contact_hy: { type: String, required: true },
  location_en: { type: String, required: true },
  location_ru: { type: String, required: true },
  location_hy: { type: String, required: true },
  hours_en: { type: String, required: true },
  hours_ru: { type: String, required: true },
  hours_hy: { type: String, required: true }
},
  {
    collection: 'contact'
  });

module.exports = mongoose.model('contact', schema, 'contact');