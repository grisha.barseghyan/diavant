var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
    email: {type: String, required: true},
  },
  {
    collection: 'subscribers'
  });

module.exports = mongoose.model('subscribers', schema, 'subscribers');