var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  id: { type: String, required: true }
},
  {
    collection: 'partners'
  });

module.exports = mongoose.model('partners', schema, 'partners');
