var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  id: { type: String, required: true },
  name: { type: String, required: true },
  product: { type: String, required: true },
},
  {
    collection: 'slider'
  });

module.exports = mongoose.model('slider', schema, 'slider');
