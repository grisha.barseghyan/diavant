var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  id: { type: String, required: true },
  name_en: { type: String, required: true },
  name_ru: { type: String, required: true },
  name_hy: { type: String, required: true },
  short_description_en: { type: String, required: true },
  short_description_ru: { type: String, required: true },
  short_description_hy: { type: String, required: true },
  description_part_one_en: { type: String, required: true },
  description_part_one_ru: { type: String, required: true },
  description_part_one_hy: { type: String, required: true },
  description_part_two_en: { type: String, required: true },
  description_part_two_ru: { type: String, required: true },
  description_part_two_hy: { type: String, required: true },
  description_part_three_en: { type: String, required: true },
  description_part_three_ru: { type: String, required: true },
  description_part_three_hy: { type: String, required: true },
  description_image_one: { type: String, required: true },
  description_image_two: { type: String, required: true },
  description_image_three: { type: String, required: true },
  probes: { type: Array, required: false },
  softwares: { type: Array, required: false },
  accessories: { type: Array, required: false },
  image: { type: String, required: false },
  thumbnail: { type: String, required: false },
  slider_image: { type: String, required: false },
  banner_image: { type: String, required: false },
  footer_image: { type: String, required: false },
  category: { type: String, required: false },
  brand: { type: String, required: false }
},
  {
    collection: 'products'
  });

schema.index({
  "name_en": 'text',
  "name_ru": 'text',
  "name_hy": 'text',
  "description_part_one_en": 'text',
  "description_part_two_en": 'text',
  "description_part_three_en": 'text',
  "description_part_one_ru": 'text',
  "description_part_two_ru": 'text',
  "description_part_three_ru": 'text',
  "description_part_one_hy": 'text',
  "description_part_two_hy": 'text',
  "description_part_three_hy": 'text',
});

module.exports = mongoose.model('products', schema, 'products');
