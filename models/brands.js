var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  id: { type: String, required: true },
  name_en: { type: String, required: true },
  name_ru: { type: String, required: true },
  name_hy: { type: String, required: true },
  long_name_en: { type: String, required: true },
  long_name_ru: { type: String, required: true },
  long_name_hy: { type: String, required: true },
  description_en: { type: String, required: true },
  description_ru: { type: String, required: true },
  description_hy: { type: String, required: true },
},
  {
    collection: 'brands'
  });

module.exports = mongoose.model('brands', schema, 'brands');
