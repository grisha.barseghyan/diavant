var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  related: { type: Array, required: true },
  url: { type: String, required: true }
},
  {
    collection: 'upload_file'
  });

module.exports = mongoose.model('upload_file', schema, 'upload_file');
