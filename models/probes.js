var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
    id: {type: String, required: true},
    en: {type: Object, required: true},
    ru: {type: Object, required: true},
    hy: {type: Object, required: true},
    img: {type: String, required: false},
  },
  {
    collection: 'probes'
  });

module.exports = mongoose.model('probes', schema, 'probes');