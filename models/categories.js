var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
    id: {type: String, required: true},
    en: {type: Object, required: true},
    ru: {type: Object, required: true},
    hy: {type: Object, required: true},
    img: {type: String, required: true},
    thumb: {type: String, required: true},
  },
  {
    collection: 'categories'
  });

module.exports = mongoose.model('categories', schema, 'categories');