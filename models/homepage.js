var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  products_description_en: { type: String, required: true },
  products_description_ru: { type: String, required: true },
  products_description_hy: { type: String, required: true },
  why_us_first_field_en: { type: String, required: true },
  why_us_first_field_ru: { type: String, required: true },
  why_us_first_field_hy: { type: String, required: true },
  why_us_first_field_description_en: { type: String, required: true },
  why_us_first_field_description_ru: { type: String, required: true },
  why_us_first_field_description_hy: { type: String, required: true },
  why_us_second_field_en: { type: String, required: true },
  why_us_second_field_ru: { type: String, required: true },
  why_us_second_field_hy: { type: String, required: true },
  why_us_second_field_description_en: { type: String, required: true },
  why_us_second_field_description_hy: { type: String, required: true },
  why_us_second_field_description_ru: { type: String, required: true },
  why_us_third_field_en: { type: String, required: true },
  why_us_third_field_ru: { type: String, required: true },
  why_us_third_field_hy: { type: String, required: true },
  why_us_third_field_description_en: { type: String, required: true },
  why_us_third_field_description_ru: { type: String, required: true },
  why_us_third_field_description_hy: { type: String, required: true },
  why_us_forth_filed_en: { type: String, required: true },
  why_us_forth_filed_ru: { type: String, required: true },
  why_us_forth_filed_hy: { type: String, required: true },
  why_us_forth_filed_description_en: { type: String, required: true },
  why_us_forth_filed_description_ru: { type: String, required: true },
  why_us_forth_filed_description_hy: { type: String, required: true },
  why_us_title_en: { type: String, required: true },
  why_us_title_ru: { type: String, required: true },
  why_us_title_hy: { type: String, required: true },
  news_description_hy: { type: String, required: true },
  news_description_ru: { type: String, required: true },
  news_description_en: { type: String, required: true },
},
  {
    collection: 'home'
  });

module.exports = mongoose.model('home', schema, 'home');
