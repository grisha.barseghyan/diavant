var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  video: { type: String, required: true },
  slider: { type: Array, required: true },
  certificates: { type: Array, required: true },
  description_en: { type: String, required: true },
  description_ru: { type: String, required: true },
  description_hy: { type: String, required: true },
  mission_en: { type: String, required: true },
  mission_ru: { type: String, required: true },
  mission_hy: { type: String, required: true },
  experience_en: { type: String, required: true },
  experience_ru: { type: String, required: true },
  experience_hy: { type: String, required: true },
  equipment_en: { type: String, required: true },
  equipment_ru: { type: String, required: true },
  equipment_hy: { type: String, required: true },
},
  {
    collection: 'about'
  });

module.exports = mongoose.model('about', schema, 'about');