var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  id: { type: String, required: true },
  name_en: { type: Object, required: true },
  name_ru: { type: Object, required: true },
  name_hy: { type: Object, required: true },
},
  {
    collection: 'accessories'
  });

module.exports = mongoose.model('accessories', schema, 'accessories');
