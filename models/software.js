var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  id: { type: String, required: true },
  name_en: { type: Object, required: true },
  name_ru: { type: Object, required: true },
  name_hy: { type: Object, required: true },
},
  {
    collection: 'software'
  });

module.exports = mongoose.model('software', schema, 'software');
