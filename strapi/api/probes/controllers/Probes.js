'use strict';

/**
 * Probes.js controller
 *
 * @description: A set of functions called "actions" for managing `Probes`.
 */

module.exports = {

  /**
   * Retrieve probes records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.probes.search(ctx.query);
    } else {
      return strapi.services.probes.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a probes record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.probes.fetch(ctx.params);
  },

  /**
   * Count probes records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.probes.count(ctx.query);
  },

  /**
   * Create a/an probes record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.probes.add(ctx.request.body);
  },

  /**
   * Update a/an probes record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.probes.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an probes record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.probes.remove(ctx.params);
  }
};
