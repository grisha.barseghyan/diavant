'use strict';

/**
 * Accessories.js controller
 *
 * @description: A set of functions called "actions" for managing `Accessories`.
 */

module.exports = {

  /**
   * Retrieve accessories records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.accessories.search(ctx.query);
    } else {
      return strapi.services.accessories.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a accessories record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.accessories.fetch(ctx.params);
  },

  /**
   * Count accessories records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.accessories.count(ctx.query);
  },

  /**
   * Create a/an accessories record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.accessories.add(ctx.request.body);
  },

  /**
   * Update a/an accessories record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.accessories.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an accessories record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.accessories.remove(ctx.params);
  }
};
